function [ Iwb ] = white_patch( I )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    sum_c = zeros(3,1); %sum of all colors by chanel (R,G,B)
    sum2_c = zeros(3,1);
    gavg = 0;
    
    maxr2 = max(max(I(:,:,1).^2));
    maxb2 = max(max(I(:,:,3).^2));
    
    maxr = max(max(I(:,:,1)));
    maxb = max(max(I(:,:,3)));
    maxg = max(max(I(:,:,2)));
    
    width = size(I, 2)
    height = size(I, 1)
    
    Iwb = zeros(height, width, 3);
    
    for i=1:height
       for j=1:width
           sum_c(2) = sum_c(2)+I(i,j,2);
       end
    end
    gavg = sum_c(2)/(width*height); %average
    
    for k=1:2:3
        for i=1:height
            for j=1:width
                sum_c(k) = sum_c(k)+I(i,j,k);
                sum2_c(k) = sum2_c(k)+(I(i,j,k)*I(i,j,k));
            end
        end
    end
    
%     syms ur vr
%     eqr1 = sum2_c(1)*ur + sum_c(1)*vr == (width*height)*gavg;
%     eqr2 = maxr2*ur + maxr*vr == maxg;
%     
%     [Ar,Br] = equationsToMatrix([eqr1, eqr2], [ur, vr]);
%     
%     Xr = linsolve(Ar,Br)
%     
%     syms ub vb
%     eqb1 = sum2_c(3)*ub + sum_c(3)*vb == (width*height)*gavg;
%     eqb2 = maxb2*ub + maxb*vb == maxg;
%     
%     [Ab,Bb] = equationsToMatrix([eqb1, eqb2], [ub, vb]);
%     
%     Xb = linsolve(Ab,Bb)
    
    
    urr =  -0.9637;
    vrr =  1.9648;
    ubb =  -0.4973;
    vbb = 1.6134;
    
    
%     Iwb(:,:,1) = (I(:,:,1).^2)*Xr(1) + I(:,:,1)*Xr(2);
%     Iwb(:,:,2) = I(:,:,2);
%     Iwb(:,:,3) = (I(:,:,3).^2)*Xb(1) + I(:,:,3)*Xb(2);

    Iwb(:,:,1) = (I(:,:,1).^2)*urr + I(:,:,1)*vrr;
    Iwb(:,:,2) = I(:,:,2);
    Iwb(:,:,3) = (I(:,:,3).^2)*ubb + I(:,:,3)*vbb;

end

