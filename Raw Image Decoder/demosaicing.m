function [ Id ] = demosaicing( cfa )
    
    width = size(cfa, 2);
    height = size(cfa, 1);
    
    Id = (zeros(height, width, 3)); %rgb matrix 
    RED = 1;
    GREEN = 2;
    BLUE = 3;
    
    
    %Bayer pattern: 
    %Red and Green pixels alternate in odd rows
    %Blue and Green pixels alternate in even rows
    %   R|G|R|G|R|G|...
    %   G|B|G|B|G|B|...
    
    %fill Id (image "demosaiced") with the cfa values for each color matrix
    %according to the bayer pattern
    for i=1:height
        for j=1:width 
            if( mod(i,2) ~= 0 )%it is a R & G row
                if( mod(j,2) ~= 0)%it is a raw red pixel
                    Id(i,j,RED) = cfa(i,j);
                else %it is a green raw pixel
                    Id(i,j,GREEN) = cfa(i,j);
                end
            else %it is a B & G row
                if( mod(j,2) == 0)%it is a blue raw pixel
                    Id(i,j,BLUE) = cfa(i,j);
                else %it is a green raw pixel
                    Id(i,j,GREEN) = cfa(i,j);
                end
            end
        end
    end
    
  
    Id(:,:,GREEN) = Id(:,:,GREEN) + imfilter(Id(:,:,GREEN), [0 1 0;1 0 1;0 1 0]/4);
    
    
    B1 = imfilter(Id(:,:,BLUE), [1 0 1;0 0 0;1 0 1]/4);
    B2 = imfilter(B1+Id(:,:,BLUE), [0 1 0;1 0 1;0 1 0]/4);
    Id(:,:,BLUE) = Id(:,:,BLUE)+B1+B2;
    
    R1 = imfilter(Id(:,:,RED), [1 0 1;0 0 0;1 0 1]/4);
    R2 = imfilter(R1+Id(:,:,RED), [0 1 0;1 0 1;0 1 0]/4);
    Id(:,:,RED) = Id(:,:,RED)+R1+R2;
    
    %normalizes the image for [0,1] real values
    %Id = mat2gray(Id);    
end

