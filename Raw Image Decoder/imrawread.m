function [cfa] = imrawread(filename)
	
	warning off MATLAB:tifflib:TIFFReadDirectory:libraryWarning 	%turns off lib warnings
	t = Tiff(filename,'r'); 										%opens the raw image for reading
	offsets = getTag(t,'SubIFD');
	setSubDirectory(t,offsets(1));
	cfa = read(t); 													%reads the image to the (color filter array) cfa
	close(t); 														%closes the image    
    
%     meta_info = imfinfo(filename);
%     x_origin = meta_info.SubIFDs{1}.ActiveArea(2)+1;
%     width = meta_info.SubIFDs{1}.DefaultCropSize(1);
%     y_origin = meta_info.SubIFDs{1}.ActiveArea(1)+1;
%     height = meta_info.SubIFDs{1}.DefaultCropSize(2);
%     cfa =(cfa(y_origin:y_origin+height-1,x_origin:x_origin+width-1));
    
    %normalizes the image for [0,1] real values
    cfa = mat2gray(cfa); 
end
