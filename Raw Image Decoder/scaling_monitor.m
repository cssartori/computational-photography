function [ Iwb ] = scaling_monitor( I )

    RED = 1;
    GREEN = 2;
    BLUE = 3;
    
    width = size(I, 2);
    height = size(I, 1);
    
    Iwb =(zeros(height, width, 3));
    
    imshow(I);
    [y,x] = ginput(1);
    close all
    
    x = round(x);
    y = round(y);
    
    Rl = I(x,y,RED);
    Gl = I(x,y,GREEN);
    Bl = I(x,y,BLUE);
    
    maxv = max(I(:));
    
    Iwb(:,:,RED) = I(:,:,RED)*(maxv/Rl);
    Iwb(:,:,GREEN) = I(:,:,GREEN)*(maxv/Gl);
    Iwb(:,:,BLUE) = I(:,:,BLUE)*(maxv/Bl);
    
end

