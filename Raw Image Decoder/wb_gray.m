function [ Iwb ] = wb_gray( I )
    
    sum_c = zeros(3,1); %sum of all colors by chanel (R,G,B)
    avg_c = zeros(3,1); %average of all colors by chanel (R,G,B)
    
    width = size(I, 2);
    height = size(I, 1);
    
    Iwb = zeros(height, width, 3);
    
    for k=1:3
        for i=1:height
            for j=1:width
                sum_c(k) = sum_c(k)+I(i,j,k);
            end
        end
        avg_c(k) = sum_c(k)/(width*height); %average
    end
       
    if(avg_c(1) == avg_c(2) && avg_c(2) == avg_c(3)) %if they are equal the "gray world" is already defined
        Iwb = I;
        return
    end
    
    alpha = avg_c(2)/avg_c(1);
    beta = avg_c(2)/avg_c(3);
    
    
    Iwb(:,:,1) = I(:,:,1)*alpha;
    Iwb(:,:,2) = I(:,:,2);
    Iwb(:,:,3) = I(:,:,3)*beta;
    
end

