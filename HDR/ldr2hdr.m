function [ hdr ] = ldr2hdr( filenames, t, C, mode)

meta_info = imfinfo(filenames(1,:));
width = meta_info.Width;
height = meta_info.Height;

I = uint8(zeros(height, width, 3, size(filenames,1)));   %image must be uint8 (0-255) so that it is possible to index the C curve
for k=1:size(filenames,1)%read images to an array
    I(:,:,:,k) = im2uint8((im2double(imread(filenames(k,:)))).^2.2); %read and apply gamma decoding
end

switch mode
    case 'generic'
        hdr = hdr_generic(I, t, C);
    case 'debevec'
        hdr = hdr_debevec(I, t, C);
end

end

%Creates the W weights for the Debevec's algorithm
function [ wz ] = generateW()
    wz = zeros(256,1);
    
    for i=1:256
        if(i <= 128)
            wz(i) = i;
        else
            wz(i) = 256 - i;
        end
    end
end

%Implementation of Debevec's algorithm for HDR image rendering from LDR
%images (I), with a given exposure time (t) for each image and a response
%curve of the camera C
function [ hdr ] = hdr_debevec(I, t, C)
    [height, width, ~, samples] = size(I);
    ln_E = zeros(height, width, 3);
    ln_t = log(t);
    w = generateW();
    
    for i=1:height
        for j=1:width
            for c=1:3
                total_lnE = 0;
                total_weight = 0;                
                for k=1:samples
                    Zij = I(i,j,c,k)+1;
                    weight = w(Zij); total_weight = total_weight + weight;
                    total_lnE = total_lnE + (weight*(C(Zij,c) - ln_t(k)));
                end
                ln_E(i,j,c) = total_lnE / total_weight;
            end
        end
    end
    
    hdr = exp(ln_E);        
    index = find(isnan(hdr) | isinf(hdr));
    hdr(index) = 0;
end

%A generic implementation of HDR image rendering applying the basic
%concepts of the algorithm.
function [ hdr ] = hdr_generic(I, t, C)
    [height, width, ~, samples] = size(I);
    Eavg = zeros(height, width, 3);
    E = zeros(height, width, 3, samples);  %E is the pixels irradiance for each image (value from C curve), must be double
    
    for i=1:height
        for j=1:width
            n=[0;0;0];
            for k=1:samples
                oldE = Eavg(i,j,:);
                oldN = n(:);
                for c=1:3
                    if(I(i,j,c,k) >= 1 && I(i,j,c,k)+1 <= 255)
                        E(i,j,c,k) = (exp(C(I(i,j,c,k)+1,c)))/t(k); 
                        Eavg(i,j,c) = Eavg(i,j,c)+E(i,j,c,k);
                        n(c) = n(c)+1;
                    else
                        Eavg(i,j,:) = oldE;
                        n = oldN;
                        break;
                    end
                end
            end
            if(n(1) ~= 0)
                Eavg(i,j,1) = Eavg(i,j,1)/n(1);
                Eavg(i,j,2) = Eavg(i,j,2)/n(2);
                Eavg(i,j,3) = Eavg(i,j,3)/n(3);
            end
        end  
    end

    hdr = Eavg;
end

