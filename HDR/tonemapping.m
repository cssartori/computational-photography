%global tonemapping by Reinhard's global operator
function [ rgb ] = tonemapping( hdr )
    [height, width, ~] = size(hdr);
    delta = 0.00000001;
    L = 0;
    Lxy = zeros(height, width);

    for i=1:height
        for j=1:width
            Lxy(i,j) = 0.299*(hdr(i,j,1))+0.587*(hdr(i,j,2))+0.114*(hdr(i,j,3));
            Lxy(i,j) = Lxy(i,j) + delta;
            L = L + log(Lxy(i,j));
        end
    end

    L = exp(L/(height*width));

    Ls = zeros(height, width);
    Lg = zeros(height, width);
    alpha = 0.18;

    for i=1:height
        for j=1:width
            Ls(i,j) = (alpha/L)*Lxy(i,j);
            Lg(i,j) = Ls(i,j) / (1+Ls(i,j));
        end
    end

    rgb = zeros(height, width, 3);
    for i=1:height
        for j=1:width
            rgb(i,j,1) = Lg(i,j)*(hdr(i,j,1)/Lxy(i,j));
            rgb(i,j,2) = Lg(i,j)*(hdr(i,j,2)/Lxy(i,j));
            rgb(i,j,3) = Lg(i,j)*(hdr(i,j,3)/Lxy(i,j));
        end
    end

    rgb = rgb.^(1/2.2);
end